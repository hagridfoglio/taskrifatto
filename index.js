const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const expressSession = require('express-session')
const app = express()

app.use(express.static('public'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.set('view engine', 'ejs')
app.use(expressSession({
    secret: "sonomrmiguardi",
    resave: false,
    saveUninitialized: true,
    cookie: {
        secure: 'auto',
        maxAge: 3600000
    }
}))

const porta = 4000

//Connessione al DB

const db = mongoose.connect("mongodb://localhost:27017/?readPreference=primary&appname=MongoDB%20Compass&directConnection=true&ssl=false", {useNewUrlParser: true}, () => {
    console.log("Sono connesso a mongo!")
})

const port = 4000
app.listen(port, () => {
    console.log(`Sono in ascolto sulla porta ${port}`)
})


global.loggato = null
global.ruolo = null

app.use("*", (req, res, next) => {
    loggato = req.session.userId
    ruolo = req.session.role

    console.log(loggato, ruolo)
    next()
})

//ROTTE
const homeController = require('./controllers/controllerHome')
const registerPageController = require('./controllers/registerPageController')
const registerActionController = require('./controllers/registerActionController')
const loginPageController = require('./controllers/loginPageController')
const loginActionController = require('./controllers/loginActionController')
const logoutController = require('./controllers/logoutController')
const subsPageAddController = require('./controllers/subsPageAddController')
const controllerCreazioneServizio = require('./controllers/controllerCreazioneServizio')
const controllerSalvataggioServizio = require('./controllers/controllerSalvataggioServizio')
const controllerDettaglioServizio = require('./controllers/controllerDettaglioServizio')







const checkAdminMiddleware = require('./middleware/checkAdminMiddleware')
const controllerSalvataggioOrdine = require('./controllers/controllerSalvataggioOrdine')

app.get("/", homeController)
app.get("/auth/register", registerPageController)
app.post("/auth/register", registerActionController)
app.get("/auth/login", loginPageController)
app.post("/auth/login", loginActionController)
app.get("/auth/logout", logoutController)
app.get("/servizio", controllerCreazioneServizio)   
app.post("/servizio", controllerSalvataggioServizio)  
app.get("/servizio/:servId", controllerDettaglioServizio)








app.get("/subscriptions/add", checkAdminMiddleware, subsPageAddController)



//------------------ non funziona questa parte
const controllerSalvataggioOrdine = require('./controllers/controllerSalvataggioOrdine')
const controllerCreazioneOrdine = require('./controllers/controllerCreazioneOrdine')

app.post("/carrello", controllerSalvataggioOrdine)
app.get("/carrello", controllerCreazioneOrdine)