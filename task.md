/**
 * Creare un sistema di gestione rinnovi di servizio.
 * Immaginate di acquistare degli abbonamenti a dominio, il gestore tiene traccia di:
 * - Servizi disponibili
 * - Utenti che effettuano gli acquisti
 * - Ordini di servizio
 * 
 * Per ogni servizio voglio tener traccia di:
 * - Codice univoco generato automaticamente - CHALLENGE : oppure no
 * - Nome del servizio
 * - Descrizione
 * - Prezzo
 * - Durata del servizio (in numero di giorni)
 * - Opzionale: FOTO
 * 
 * Per ogni utente che acquista un servizio voglio tener traccia di:
 * - Anagrafica utente
 * - Codice univoco generato automaticamente - CHALLENGE : oppure no
 * 
 * Per ogni ordine voglio tener traccia di:
 * - Il servizio scelto
 * - L'utente che lo ha richiesto
 * - La data di acquisto
 * - Come è stato effettuato il pagamento (Bonifico/PayPal/Contanti)
 * - Note
 * 
 * UTENTE:
 * 1. Un utente accede al portale e consulta l'elenco dei servizi (senza autenticazione)
 * 2. L'utente per acquistare (associare) un servizio deve essere loggato!
 * 3. Un utente può consultare sulla sua pagina personale di riepilogo tutti i suoi ordini che comprendono i dettagli del servizio
 *    la data di acquisto e quanto manca alla scadenza del servizio!
 * 
 * ADMIN:
 * 1. CRUD dei servizi
 * 2. RUD Utenti
 * 3. CRUD ordini
 * 
 * ***** Potete prevedere nel campo di registrazione se sei un ADMIN o UTENTE
 */