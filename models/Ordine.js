
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const OrdineSchema = new Schema(
    {
        utente: String,
        Servizio: String,
        prezzo: String,
        datePosted: {
            type: Date,
            default: new Date()
        },
        pagamento: String,
        note: String,
    
       
    }
)

const Ordine = mongoose.model('Ordine', OrdineSchema)

module.exports = Ordine