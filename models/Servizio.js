
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ServizioSchema = new Schema(
    {
        titolo: String,
        descrizione: String,
        scadenza: String,
        prezzo: String,
       
       
    }
)

const Servizio = mongoose.model('Servizio', ServizioSchema)

module.exports = Servizio