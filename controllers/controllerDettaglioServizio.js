const Servizio = require('../models/Servizio')

module.exports = async (req, res) => {
    try {
        let abbonamento = await Servizio.findById(req.params.servId)

        res.render('Servizio', {
            servizio: abbonamento
        })
    } catch (error) {
        res.end("Errore del server (sto nel dettaglio)")
    }
}