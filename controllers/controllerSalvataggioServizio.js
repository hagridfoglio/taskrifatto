const Servizio = require('../models/Servizio')


module.exports = async (req, res) => {

    

    let nuovoServizio = {
        titolo: req.body.inputTitolo,
        descrizione: req.body.inputDescrizione,
        scadenza: req.body.inputScadenza,
        prezzo: req.body.inputPrezzo, 
       
    }

    let nuovoProdotto = await Servizio.create(nuovoServizio)

    if(!nuovoProdotto){
        res.end("Errore di inserimento")
    }
    else{
        res.redirect(`/servizio/${nuovoProdotto._id}`)
    }

}
        
    
