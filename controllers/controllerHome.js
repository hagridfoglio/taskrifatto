const Servizio = require('../models/Servizio')

module.exports = async (req, res) => {          //Dichiaro la arrow function come asincrona
    // console.log(req.session)

    try {
        let elenco = await Servizio.find({})    //Attendi il risultato della find e non vai avanti finché non risponde!    
        
        res.render('home', {
            elencoservizi: elenco              //Passo un oggetto al mio EJS
        })
    } catch (error) {
        res.end("Errore del server (sto nella home)")
    }
}