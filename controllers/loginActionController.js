const User = require("../models/User")
const bcrypt = require('bcrypt')

module.exports = (req, res) => {

    let utente = {
        username: req.body.inputUser.toUpperCase(),
        password: req.body.inputPassword
    }

    if(!utente.username || !utente.password){
        res.render('error', {
            details: "Errore di validazione, inserisci i campi obbligatori"
        })
    }
    else{
        User.findOne({
            username: utente.username
        }, (err, userFound) => {
            if(!err && userFound){
                bcrypt.compare(utente.password, userFound.password, (err, same) => {
                    if(same){
                        req.session.userId = userFound._id
                        req.session.role = userFound.role

                        res.redirect("/")
                    }
                    else{
                        res.render('error', {
                            details: "Errore, password errata!"
                        })
                    }
                })
            }
            else{
                res.render('error', {
                    details: "Errore, utente non trovato!"
                })
            }
        })
    }
}